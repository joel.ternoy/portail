# fonction outil spécifique à reducer1
def number_of_a(chaine):
    result = 0
    for c in chaine:
        if c == 'a':
            result = result + 1
    return result

# fonctions de "l'interface" reduce
def prepare(list):
    '''
    @param {list(chaine)}  list : la liste des chaînes à préparer
    @return {list(int)} : liste du nombre de 'a' dans chacune des chaines de list
    >>> prepare(["timoleon", "abracadabra", "banane", "abc"]) ==   [0,5,2,1]
    True
    '''
    return [ number_of_a(chaine)  for chaine in list]
    
def execute(list):
    '''
    @param {list(int)} list : une liste d'entiers
    @return {int} la somme des valeurs de list²
    >>> execute([0,5,2,1]) == 8
    True
    '''
    return sum(list)




    
if __name__ == '__main__':
    import doctest
    doctest.testmod()